---
output: pdf_document
---
# Predict genome wide TOP2B signal

This script generates <i>de novo</i> TOP2B binding virtual maps using a generalized model trained with CTCF, RAD21 and DNase-seq genomic signals. The genome is first chopped into 300 bp windows and the genomic scores of before signals are computed using the function `get.signals.score` defined in `../1_generate_learning_dataset/create_model_matrix.Rmd`. It also makes use of the `Bioconductor` libraries `regioneR`, `rtracklayer`, `randomForest` and `BSgenome.Mmusculus.UCSC.mm9`. We start by defining a set of helper functions:
```{r, echo=FALSE}
get.signals.score <- function(gr, signals.tab){
  signal.list <- list()
  for(i in 1:nrow(signals.tab)){
    signal <- signals.tab$Name[i]
    assay <- signals.tab$Assay[i]
    data.file <- signals.tab$Data_file[i]
    if(assay != "WGBS"){
      suppressMessages(library(bamsignals))
      counts <- as.integer(bamCount(as.character(data.file), gr, verbose = F))
      # library size
      nS <- signals.tab[i,]$Library_size
      # if RNA-seq/MNase-seq or DNAse-seq, just normalized counts
      if(assay %in% c("RNA-seq","DNase-seq","MNase-seq")) signal.list[[i]] <- 1e8*(counts/nS)
      # if ChIP-seq, normalized by input signal and library size as in Comoglio and Paro (Plos Comp Biol, 2014)
      if(!assay %in% c("RNA-seq","DNase-seq","MNase-seq")){
        # Normalize by input
        input.file <- signals.tab$Control_data_file[i]
        counts.input <- as.integer(bamCount(as.character(input.file), gr, verbose = F))
        nI <- signals.tab[i,]$Control_library_size
        m <- min(nI, nS)
        signal.list[[i]] <- unlist(lapply(1:length(counts),function(x){
            xSR <- counts[x]
            xIR <- counts.input[x]
            up <- (xSR/nS)*m + 1
            down <- (xIR/nI)*m + 1
            unname(log2(up/down))
        }))
      }
    }
    if(assay == "WGBS"){
      # if CpG methylation signal, we define the score as the density of methylated CpGs, as in Rube et al (Nat
      # Comm, 2016). We slightly modify such definition to include percentages of methylation of each CpG, since
      # we have the data in such format, note that unique(width(gr)) is necessarily of length = 1 (see main code)
      suppressMessages(library(Biostrings))
      # CpG methylation data in MEFs from Yu et al (PNAS, 2014) have been previously converted to GRanges
      met.cg <- get(load(as.character(data.file)))
      cg.freq <- dinucleotideFrequency(getSeq(BSgenome.Mmusculus.UCSC.mm9, gr), step = 1)[,"CG"]/unique(width(gr))
      ov <- findOverlaps(gr, met.cg)
      query.idxs <- unique(queryHits(ov))
      # we set uncovered loci to 0, cuz metCpG data doesnt cover the whole genome
      met.val <- rep(0, length(gr))
      met.val[query.idxs] <- unlist(lapply(unique(queryHits(ov)),function(idx){
        subj.idx <- subjectHits(ov[queryHits(ov) %in% idx])
        sum(met.cg$score[subj.idx])/(length(subj.idx)*100)
      }))
      signal.list[[i]] <- cg.freq*met.val*1000
    }
  }
	names(signal.list) <- as.character(signals.tab$Name)
	return(signal.list)
}
```
```{r}
# BEGIN helper functions
gr.subtract.overlapping <- function(gr1, gr2, cutoff=1, ignore.strand=T){
        gr.subtract.overlapping.cutoff1 <- function(gr1, gr2, maxgap=0, ignore.strand=T){
                ov <- findOverlaps(gr1, gr2, maxgap=maxgap, ignore.strand=ignore.strand)
                if(length(queryHits(ov)) > 0) return(gr1[-unique(queryHits(ov))])
                return(gr1)
        }
        if(is.null(gr2)) return(gr1)
        if(cutoff > 1 | cutoff <= 0) stop(paste("\nCutoff should be a number > 0 and <= 1!",
                                                sep = ""))
        if(cutoff == 1) return(gr.subtract.overlapping.cutoff1(gr1, gr2,
                                                ignore.strand=ignore.strand))
        if(cutoff < 1){
                out1 <- gr.subtract.overlapping.cutoff1(gr1, gr2,
                                                ignore.strand=ignore.strand)
                tmp <- gr.keep.overlapping(gr1, gr2, ignore.strand=ignore.strand)
                if(length(tmp) == 0) return(tmp)
                ## we iterate over tmp and look for overlaps with cutoff >= cutoff
                keep = unlist(lapply(1:length(tmp),function(i){
                        res <- F
                        sub <- subtractRegions(tmp[i], gr2)
                        if(sum(width(sub))/width(tmp[i]) >= cutoff) res <- T
                        res
                }))
                out2 <- tmp[keep]
                return(c(out1, out2))
        }
}
get.prediction.list <- function(models.path, test.mat){
        out <- list()
        model.files <- list.files(models.path, full.names=T, recursive=F, pattern="RData$")
        i <- 0
        suppressMessages(library(randomForest))
        for(f in model.files){
                i <- i+1
                message("\tPredicting sites (model", i, ")")
                r <- get(load(f))
                out[[i]] <- as.data.frame(predict(r, test.mat, type="prob"))
        }
        return(out)
}
avge.prob.cross.val <- function(pred.list, posit.class){
        mat <- matrix(ncol=length(pred.list), nrow=nrow(pred.list[[1]]))
        for(j in 1:length(pred.list)) mat[,j] <- pred.list[[j]][,posit.class]
        return(rowMeans(mat))
}
# END helper functions
```

For illustration purposes, we will predict TOP2B signal in chromosome 19 of MEFs. For predictions along the whole genome, we strongly recommend to parallelize the execution (f.i. by chromosomes) using a multithreading system. To only consider the mappable genome, we get rid of ENCODE non mappable, blacklisted loci and masked regions of the mouse genome:
```{r}
# required libraries
suppressMessages(library(BSgenome.Mmusculus.UCSC.mm9))
suppressMessages(library(regioneR))
suppressMessages(library(rtracklayer))
# define genome (chr19)
genome <- getGenome(BSgenome.Mmusculus.UCSC.mm9);
genome <- genome[seqnames(genome) == "chr19"]
names(genome) <- "chr19"
# identify blacklisted, not mappable and masked regions to avoid them
NOT.mappab <- subtractRegions(genome,
  import.bedGraph("../data/mouse_annotations/chr19_wgEncodeCrgMapabilityAlign36mer_score1.bedgraph.gz"))
mask <- mergeRegions(import.bed("../data/mouse_annotations/mm9-blacklist.bed.gz"),
  mergeRegions(get(load("../data/mouse_annotations/default.mm9.mask.RData")),
  NOT.mappab))
mask <- mask[seqnames(mask) == "chr19"]
# chop genome in 300 bp windows and remove those overlaping more than 20% with blacklisted,
# not mappable and masked regions. For ilustration purposes, cutoff = 1
slides <- unname(unlist(slidingWindows(genome, width=300, step=300)))
slides <- slides[width(slides) == 300]
## remove masks
mappab.slides <- gr.subtract.overlapping(slides, mask, cutoff=1)
```

Then we score CTCF, RAD21 and DNAse-seq genomic signals within the 300 bp windows and apply the TOP2B binding model to obtain a probability, which we attach to the chromosome 19 `GRanges` object and export as a bedgraph file.
```{r, error=FALSE,warning=FALSE,message=FALSE}
# signals tab
tab <- read.csv("../data/bam_files/MEF/MEF_bam_info_chr19.csv", h=T, sep="\t")
tab <- tab[tab$Target %in% c("CTCF","RAD21") | tab$Assay=="DNase-seq",]
tab
# get scores
slides.score <- data.frame(get.signals.score(mappab.slides, tab))
head(slides.score)
# rename to adapt model names
colnames(slides.score) <- c("CTCF","DNase_seq","RAD21")
# apply trained model on aB, liver and MEF
pred <- get.prediction.list("../data/trained_models/CTCF_RAD21_DNase_mouse_model",
                            slides.score)
prob.avges <- avge.prob.cross.val(pred, posit.class="peaks")
# assign prob to ranges
mappab.slides$prob <- prob.avges
mappab.slides
# assign seqlengths
seqlengths(mappab.slides) <- seqlengths(getGenome("mm9"))[names(seqlengths(mappab.slides))]
# export bedgraph
dir.create("predictions")
write.table(as.data.frame(mappab.slides)[,c("seqnames","start","end","strand","prob")],
          file=paste("predictions/MEF_chr19_preds.bg", sep=""),
          col.names=F, row.names=F, quote=F, sep="\t")
```