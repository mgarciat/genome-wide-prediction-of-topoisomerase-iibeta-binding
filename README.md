# Genome-wide prediction of TOP2B binding

Here we provide the code to reproduce the analyses performed in our study entitled [*Genome-wide prediction of topoisomerase IIβ binding by architectural factors and chromatin accessibility*](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007814), in which we applied machine learning to generate virtual TOP2B ChIP-seq tracks in mammals. Predictive tracks are available in the following UCSC public sessions: [mouse predictions](https://genome.ucsc.edu/s/peripecias/Martinez_Garcia_et_al_2020_mouse_predictions), [human predictions](https://genome.ucsc.edu/s/peripecias/Martinez_Garcia_et_al_2020_human_predictions).

The code has been structured in three sections:

1. Generation of learning datasets
2. Machine learning models training
3. Genome wide predictions

For further details of sections 1 and 3, contact P.M. Martínez (<pmmargar@upo.es>). For further details of section 2, contact M. García Torres (<mgarciat@upo.es>).
