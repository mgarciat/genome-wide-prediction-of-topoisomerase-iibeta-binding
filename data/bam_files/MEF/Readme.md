## Bam files

This directory should contain the bam files corresponding to each of the genomic datasets used for TOP2B binding training. Due to storing size limitations, no file could be uploaded. However, information on the corresponding accession of each sample are detailed in the uploaded table. Note that, for illustration purposes, only chromosome 19 in MEFs is considered. Already processed bam files will be provided under request.


