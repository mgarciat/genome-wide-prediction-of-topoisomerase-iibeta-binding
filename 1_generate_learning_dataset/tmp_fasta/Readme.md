## Temporal fasta files

To infer DNA shape features, we make use of the R library [*DNAShapeR*](https://www.bioconductor.org/packages/release/bioc/html/DNAshapeR.html), which generates temporal fasta files derived from TOP2B and random regions. Due to GitLab maximum file size of 10MiB, we were not able to upload these files.