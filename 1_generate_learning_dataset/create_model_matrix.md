# Create TOP2B binding model matrix from TOP2B/GC-random peaks and sequencing data

This script makes use of TOP2B peaks and GC-corrected random regions to generate a model matrix to be trained for TOP2B genome-wide binding prediction. Genomic scores within before loci are derived both from 15 high-throughput sequencing experiments and from their DNA sequences using `DNAshapeR` (<a href="https://www.bioconductor.org/packages/release/bioc/html/DNAshapeR.html" target="_blank">Chiu et al, 2016</a>). Besides `DNAshapeR`, this code relies in the `Bioconductor` libraries `GenomicRanges`, `bamsignals`, `Biostrings` and `BSgenome.Mmusculus.UCSC.mm9`. We start by defining a set of helper functions:
```{r}
# BEGIN helper functions
get.signals.score <- function(gr, signals.tab){
  signal.list <- list()
  for(i in 1:nrow(signals.tab)){
    signal <- signals.tab$Name[i]
    assay <- signals.tab$Assay[i]
    data.file <- signals.tab$Data_file[i]
    if(assay != "WGBS"){
      suppressMessages(library(bamsignals))
      counts <- as.integer(bamCount(as.character(data.file), gr, verbose = F))
      # library size
      nS <- signals.tab[i,]$Library_size
      # if RNA-seq/MNase-seq or DNAse-seq, just normalized counts
      if(assay %in% c("RNA-seq","DNase-seq","MNase-seq")) signal.list[[i]] <- 1e8*(counts/nS)
      # if ChIP-seq, normalized by input signal and library size as in Comoglio and Paro (Plos Comp Biol, 2014)
      if(!assay %in% c("RNA-seq","DNase-seq","MNase-seq")){
        # Normalize by input
        input.file <- signals.tab$Control_data_file[i]
        counts.input <- as.integer(bamCount(as.character(input.file), gr, verbose = F))
        nI <- signals.tab[i,]$Control_library_size
        m <- min(nI, nS)
        signal.list[[i]] <- unlist(lapply(1:length(counts),function(x){
            xSR <- counts[x]
            xIR <- counts.input[x]
            up <- (xSR/nS)*m + 1
            down <- (xIR/nI)*m + 1
            unname(log2(up/down))
        }))
      }
    }
    if(assay == "WGBS"){
      # if CpG methylation signal, we define the score as the density of methylated CpGs, as in Rube et al (Nat
      # Comm, 2016). We slightly modify such definition to include percentages of methylation of each CpG, since
      # we have the data in such format, note that unique(width(gr)) is necessarily of length = 1 (see main code)
      suppressMessages(library(Biostrings))
      # CpG methylation data in MEFs from Yu et al (PNAS, 2014) have been previously converted to GRanges
      met.cg <- get(load(as.character(data.file)))
      cg.freq <- dinucleotideFrequency(getSeq(BSgenome.Mmusculus.UCSC.mm9, gr), step = 1)[,"CG"]/unique(width(gr))
      ov <- findOverlaps(gr, met.cg)
      query.idxs <- unique(queryHits(ov))
      # we set uncovered loci to 0, cuz metCpG data doesnt cover the whole genome
      met.val <- rep(0, length(gr))
      met.val[query.idxs] <- unlist(lapply(unique(queryHits(ov)),function(idx){
        subj.idx <- subjectHits(ov[queryHits(ov) %in% idx])
        sum(met.cg$score[subj.idx])/(length(subj.idx)*100)
      }))
      signal.list[[i]] <- cg.freq*met.val*1000
    }
  }
	names(signal.list) <- as.character(signals.tab$Name)
	return(signal.list)
}
get.shape.predictions <- function(shape.names, fasta){
  preds <- lapply(shape.names,function(shape){getShape(fasta, shapeType = shape)[[shape]]})
	names(preds) <- shape.names
	return(preds)
}
get.shape.matrices <- function(shape.names, preds){
  first <- T
  for(shape in shape.names){
    for(n.order in 1:2){
      tmp <- normalizeShape(encodeNstOrderShape(n.order, preds[[shape]], shape), n.order, shape, normalize = T)
      colnames(tmp) <- paste("k_", n.order, shape, "_", 1:ncol(tmp), sep = "")
      if(!first) mat.shape <- cbind(mat.shape, as.data.frame(tmp)) else {mat.shape <- as.data.frame(tmp);first <- F}
    }
  }
  return(mat.shape)
}
# END helper functions
```
```{r, echo=FALSE}
encodeNstOrderShape <- function(n, shapeMatrix, shapeType){
    # assign average value to NA
    shapeMatrix[is.na(shapeMatrix)] <- mean(shapeMatrix, na.rm=T)

    singleSeq <- FALSE
    if( nrow(shapeMatrix)[1] == 1 )
        singleSeq <- TRUE

    # trim both 2 bps end
    if( shapeType == "MGW" || shapeType == "ProT" || shapeType == "EP" ||
        shapeType == "Stretch" || shapeType == "Buckle" ||
        shapeType == "Shear" || shapeType == "Opening" ||
        shapeType == "Stagger" ){
            shapeMatrix <- shapeMatrix[, -c(1, 2, ncol( shapeMatrix )-1,
                ncol( shapeMatrix ))]

    }else if( shapeType == "Roll" || shapeType == "HelT" ||
            shapeType == "Tilt" || shapeType == "Rise" ||
            shapeType == "Shift" || shapeType == "Slide" ){
      shapeMatrix <- shapeMatrix[, -c(1, ncol( shapeMatrix ))]
    }

    if(singleSeq)
        shapeMatrix <- t(shapeMatrix)

    # encode k-st feature
    featureVector <- NULL
    if( n == 1 ){
        featureVector = shapeMatrix

    }else{
        m <- ncol( shapeMatrix )
        # normalization
        shapeMatrix <- normalizeShape( featureVector = shapeMatrix,
                                       thOrder = 1, shapeType = shapeType,
                                       normalize = TRUE )

        for ( i in 1 : ( m-n+1 )){
            feature <- shapeMatrix[, i]
            for ( j in ( i+1 ) : ( i+n-1 ) )
                feature <- feature * shapeMatrix[, j]

            featureVector <- cbind( featureVector, unlist( feature ) )
        }
    }

    return (featureVector)
}
normalizeShape <- function(featureVector, thOrder, shapeType, normalize){
    this.normalize <- function(x,max,min){return((x-min)/(max-min))}
 
    tableName <- c("MGW", "ProT", "Roll", "HelT", "EP", "Stretch", "Tilt", "Buckle",
                    "Shear", "Opening", "Rise", "Shift", "Stagger", "Slide")
    minTable <- c( 2.85, -16.51, -8.57, 30.94, -13.59, -0.05, -3.02, -9.26,
                    -0.3, -3.71, 3.05, -0.42, -0.42, -1.95 )
    maxTable <- c( 6.2, -0.03, 8.64, 38.05, -4.47, 0.04, 5.4, 7.66, 0.28, 1.06,
                    3.74, 0.51, 0.2, -0.97 )
    names( minTable ) <- tableName
    names( maxTable ) <- tableName

    if ( normalize  ){
        if( thOrder == 1){
            featureVector <- this.normalize( featureVector, maxTable[shapeType],
                                        minTable[shapeType] )
        }else{
            featureVector <- this.normalize( featureVector, max(featureVector),
                                        min(featureVector) )
        }
    }
    return(featureVector)
}
this.normalize <- function( x, max, min ){
  return ( (x-min)/(max-min) )
}
```
Then we load previously computed TOP2B peaks and GC-corrected random regions, which we stored in the folder `../data/learning_peaks/`. For illustration purposes and to reduce computation time, only data from MEFs in chromosome 19 is considered. We also load a data table containing information on the sequencing experiments to be scored. Bam files and corresponding indexes were stored in `../data/bam_files/`. Then, we call the helper function `get.signals.score`, which receives before table and a set of genomic regions (GRanges object) and score the set of sequencing features within such regions.
```{r}
# required libraries
suppressMessages(library(GenomicRanges))
suppressMessages(library(BSgenome.Mmusculus.UCSC.mm9))
# load learning peaks
top2 <- get(load("../data/learning_peaks/MEF_TOP2B_peaks_chr19.RData"))
gcRand <- get(load("../data/learning_peaks/MEF_GC_rand_chr19.RData"))
# print them and show they are 300 bp length
top2
gcRand
summary(width(top2))
summary(width(gcRand))
# load signals data table
tab <- read.csv("../data/bam_files/MEF/MEF_bam_info_chr19.csv", h=T, sep="\t")
head(tab)
# get scores for top2b peaks and random regions using helper function get.signals.score
top2.signals.df <- data.frame(get.signals.score(top2, tab))
gcRand.signals.df <- data.frame(get.signals.score(gcRand, tab))
head(top2.signals.df)
head(gcRand.signals.df)
# show boxplots for some datasets
par(cex.lab=1.4,cex.axis=1.1,mfrow=c(1,4))
for(j in c(1,3:5)) boxplot(top2.signals.df[,j], gcRand.signals.df[,j], names = c("TOP2","RAND"),
                           main=unlist(strsplit(colnames(top2.signals.df)[j],"MEF_"))[2],
                           col=c("firebrick3","gray60"), cex.main=1.8, ylab=ifelse(j==1,"Sequencing score",""))
```

Once we have scored the sequencing experiments, we predict DNA shape features using `DNAshapeR` (<a href="https://www.bioconductor.org/packages/release/bioc/html/DNAshapeR.html" target="_blank">Chiu et al, 2016</a>). We compute first and second order vectors for 13 shape features as described in (<a href="https://academic.oup.com/nar/article/45/22/12877/4634013" target="_blank">Li et al, 2017</a>).
```{r, error=FALSE,warning=FALSE,message=FALSE}
# dnashaper library
suppressMessages(library(DNAshapeR))
# generate fasta for TOP2B and GC-random regions
dir.create("tmp_fasta")
getFasta(top2, BSgenome.Mmusculus.UCSC.mm9, filename = "tmp_fasta/tmp_top2.fa", width = 300)
getFasta(gcRand, BSgenome.Mmusculus.UCSC.mm9, filename = "tmp_fasta/tmp_gcRand.fa", width = 300)
# we consider 13 shape features
shape.arr <- c("ProT","HelT","MGW","Roll","Rise","Shift","Slide","Tilt","Buckle","Opening","Shear","Stagger","Stretch")
# get predictions using helper function get.shape.predictions
pred.top2 <- get.shape.predictions(shape.names=shape.arr, fasta="tmp_fasta/tmp_top2.fa")
pred.gcRand <- get.shape.predictions(shape.names=shape.arr, fasta="tmp_fasta/tmp_gcRand.fa")
# extract 1st and 2nd order features andcreate matrix with helper function get.shape.matrices
top2.shape.df <- get.shape.matrices(shape.names=shape.arr, preds=pred.top2)
gcRand.shape.df <- get.shape.matrices(shape.names=shape.arr, preds=pred.gcRand)
top2.shape.df[1:5,1:5]
gcRand.shape.df[1:5,1:5]
```
Finally, we compute 1-mers, 2-mers and 3-mers using the `DNAshapeR` function `encodeSeqShape` and concatenate this with above data to create the final model matrices.
```{r,warning=FALSE}
# extract DNA sequence 1-mers,2-mers and 3-mers
top2.seq.df <- encodeSeqShape(fastaFileName="tmp_fasta/tmp_top2.fa",
                              featureNames=c("1-mer","2-mer","3-mer"), normalize=T)
gcRand.seq.df <- encodeSeqShape(fastaFileName="tmp_fasta/tmp_gcRand.fa",
                                featureNames=c("1-mer","2-mer","3-mer"), normalize=T)
colnames(top2.seq.df) <- colnames(gcRand.seq.df) <- c(paste("k_1mer_", 1:1200, sep=""),
                                                      paste("k_2mer_", 1:4784, sep=""),
                                                      paste("k_3mer_", 1:19072, sep=""))
# model matrix
top2.mat <- cbind(top2.seq.df,cbind(top2.shape.df,top2.signals.df))
top2.mat$CLASS <- "TOP2B"
gcRand.mat <- cbind(gcRand.seq.df,cbind(gcRand.shape.df,gcRand.signals.df))
gcRand.mat$CLASS <- "GC_RANDOM"
model.mat <- rbind(top2.mat,gcRand.mat)
# save matrix for machine learning training
dir.create("model_matrices")
write.table(model.mat, file = paste("model_matrices/MEF_chr19_model_matrix.csv", sep=""),
            quote=F, col.names=T, row.names=F, sep = "\t")
```